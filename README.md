# Build your webradio

This workshop works you through the different steps of setting up a webradio.

## Software

- [Liquidsoap](https://www.liquidsoap.info/) is a programming language that allows us manipulate audio streams.
- [Icecast](https://wiki.xiph.org/Icecast) is a program that allows us to take a stream and broadcast it to a lot of people.
- [BUTT](https://danielnoethen.de/butt/) is a program that allows us to take our microphone input and broadcast it to everyone!
